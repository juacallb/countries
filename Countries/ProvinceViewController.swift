//
//  ProvinceViewController.swift
//  Countries
//
//  Created by Juacall Bernard on 3/11/20.
//  Copyright © 2020 mindbody. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class ProvinceViewController: UIViewController , NetworkCallbackDelegate, UITableViewDelegate, UITableViewDataSource
{
    
    
    //MARK: Variables
    var countryID:String?
    var countryName:String?
    
    private var provinces:[Province]?;
    private let locationManager = LocationManager();//Intializing will request permissions
        
    @IBOutlet weak var provinceTable: UITableView!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var mapViewHeightContstraint: NSLayoutConstraint!
    
    // MARK: Overridden Functions
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationItem.title = countryName
        mapViewHeightContstraint.constant = (self.parent?.view.frame.height)!/3; // Makes the mapview a 3rd of the screen. To preventing hardcoded values. For cases where devices have diffrent screen sizes.
        Loading.start()
        provinceTable.delegate = self;
        fetchProvinceData();
    }
    
    //MARK: Functions
    
    func fetchProvinceData(){
        getLocation(forPlaceCalled: countryName!,zoom: 8.5) // Added here so on retry because of error it will move to country.
        let request = NetworkRequest.init(delegate: self);
        request.getProvinces(countryID:countryID!)
    }
    
    func showNoProvincesMessage(){
        let alertController = UIAlertController(title: "No Provinces", message:
              "This country does not have provinces", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          alertController.addAction(UIAlertAction(title: "Go Back", style: .default, handler: {(action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)// Navagation Controller exits current controller.
          }))

          self.present(alertController, animated: true, completion: nil)
    }
    
    func showNoCoordinatesError(){
        let alertController = UIAlertController(title: "Sorry", message:
                    "Coordinates for this location can not be found", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

                self.present(alertController, animated: true, completion: nil)
    }
    
    func showError(error: Error){
        // Always make UI changes on main thread
         DispatchQueue.main.async {
            self.provinceTable.isHidden = true;
            self.errorLabel.isHidden = false;
            self.errorLabel.text = error.localizedDescription;
            self.retryButton.isHidden = false;
        }
    }
    
    func hideError(){
        // Always make UI changes on main thread
         DispatchQueue.main.async {
            self.errorLabel.isHidden = true;
            self.errorLabel.text = "";
            self.retryButton.isHidden = true;
        }
    }
    //MARK:Action Methods
    @IBAction func retryButtonClicked(_ sender: Any) {
        hideError();
        Loading.start()
        fetchProvinceData()
    }
    
    //MARK: Network Callback Delegate Methods
    
    func didFinishDownloading(_ data: Data, type: String) {
        if(type == "provinces"){
            Loading.stop()
            do{
                provinces = try  JSONDecoder().decode([Province].self, from: data)
                if(provinces!.count != 0){
                    DispatchQueue.main.async { //Making sure UI changes and data changes are on main thread to prevent delays.
                        self.provinceTable.isHidden = false;
                        self.provinceTable.dataSource = self;
                        self.provinceTable.reloadData();
                    }
                }else{// Show message that country does not have provinces
                     DispatchQueue.main.async {
                        self.showNoProvincesMessage();
                    }
                }
            } catch {
                print(error);// Print error to log as well
                self.showError(error: error);
                
            }
        }
    }
    
    func didFinishWithError(_ error: Error, type: String) {
        Loading.stop()
        self.showError(error: error);
    }
    
    //MARK: TableView Data Source Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(provinces != nil){
            return provinces!.count;
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Custom cell not needed using basic cell
        let cell = tableView.dequeueReusableCell(withIdentifier:"provinceCell", for: indexPath);
        
        let row = indexPath.row;
        cell.textLabel?.text = provinces![row].name;
        
        return cell;
    }
    
    
    //MARK: TableView Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
           
        let provinceName = provinces![indexPath.row].name;
        getLocation(forPlaceCalled: provinceName + " , " + countryName! );
    }
    
    //MARK: Map Methods
    func getLocation(forPlaceCalled name: String, zoom:Double = 3) {
           
           let geocoder = CLGeocoder()
          //Uses the name of a place to get coordinates
           geocoder.geocodeAddressString(name) { placemarks, error in
               
               guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)");
                  DispatchQueue.main.async {
                    self.showNoCoordinatesError();
                                 }
                   return
               }
               
               guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil");
                self.showNoCoordinatesError();
                   return
               }
               
               guard let location = placemark.location else {
                print("*** Error in \(#function): placemark is nil");
                  DispatchQueue.main.async {
                                     self.showNoCoordinatesError();
                                 }
                   return
               }
            self.zoomToLocation(coordinate: location.coordinate,zoom: zoom)
            
          
           }
       }
    
    func zoomToLocation(coordinate:CLLocationCoordinate2D, zoom:Double = 3){
        let span = MKCoordinateSpan.init(latitudeDelta:zoom, longitudeDelta:zoom)// Controls Zoom
        let region = MKCoordinateRegion.init(center: coordinate, span: span)
        DispatchQueue.main.async{
            self.mapView.setRegion(region, animated: true)
        }
    }
}



struct Province{
    let id:NSInteger
    let countryCode:String
    let code:String
    let name:String
}

extension Province: Decodable { // Mapping json values to struct for use with JSON Decoder
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case countryCode = "CountryCode"
        case code = "Code"
        case name = "Name"
        
    }
}
