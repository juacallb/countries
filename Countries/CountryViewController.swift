//
//  ViewController.swift
//  Countries
//
//  Created by Juacall Bernard on 3/11/20.
//  Copyright © 2020 mindbody. All rights reserved.
//

import UIKit

class CountryViewController: UIViewController, NetworkCallbackDelegate, UITableViewDelegate , UITableViewDataSource {
   
    //TODO: Create error view to be reused
    //DONE: Create loading view to be reused;
    
    
    // MARK: Variables
    
    @IBOutlet weak var errorText: UILabel!
    
    @IBOutlet weak var retryButton: UIButton!
        
    @IBOutlet weak var countryTable: UITableView!
    
    private var selectedCountryId:String?
    
    private var selectedCountryName:String?
    
    private  var refreshControl: UIRefreshControl?
    
    private var countries:[Country]?
    
    //MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationItem.title = "Countries"
        Loading.start()
        countryTable.delegate = self;
        fetchCountryData();
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ProvinceViewController{
            vc.countryID = selectedCountryId;
            vc.countryName = selectedCountryName;
        }
    }
    
    
    // MARK: Functions
    
    func fetchCountryData(){
        hideError();//Reset View State
        countries?.removeAll();  //Remove old data
        countryTable.dataSource = nil; //Update UI to show removed data

        let request = NetworkRequest.init(delegate: self);
        request.getCountries();

    }
    
    func showError(error: Error){
        // Always make UI changes on main thread
         DispatchQueue.main.async {
            self.countryTable.isHidden = true;
            self.errorText.isHidden = false;
            self.errorText.text = error.localizedDescription;
            self.retryButton.isHidden = false;
        }
    }
    
    func hideError(){
        // Always make UI changes on main thread
         DispatchQueue.main.async {
            self.errorText.isHidden = true;
            self.errorText.text = "";
            self.retryButton.isHidden = true;
        }
    }
    
    // MARK: Actions
    @objc private func refreshCountryData(_ sender: Any) {
        refreshControl?.beginRefreshing()
        fetchCountryData()
    }
    
    @IBAction func retryButtonClicked(_ sender: Any) {
        Loading.start()
        fetchCountryData()
    }
    
    // MARK: Network Callback Delegate Methods
    func didFinishDownloading(_ data: Data, type: String) {
        if(type == "countries"){
            Loading.stop()
            do{
             countries = try  JSONDecoder().decode([Country].self, from: data)
                DispatchQueue.main.async {
                    self.countryTable.isHidden = false;
                    self.countryTable.dataSource = self;
                    self.countryTable.reloadData();
                    
                    // Configure Refresh Control on UI Thread if nil and when we are sure we have data.
                    if(self.refreshControl == nil){
                        self.refreshControl = UIRefreshControl();
                        self.refreshControl!.addTarget(self, action: #selector(self.refreshCountryData(_:)), for: .valueChanged)
                        self.countryTable.refreshControl = self.refreshControl
                    }else{
                        self.refreshControl?.endRefreshing();
                    }
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    self.countryTable.isHidden = true;
                    self.showError(error: error)
                }
            }
        }
     }
     
    func didFinishWithError(_ error: Error, type:String) {
        Loading.stop()
        print(error);
        if(type == "countries"){
            showError(error: error);
        }
     }
    
    
    // MARK: Table View Data Source Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(countries != nil){
            return countries!.count;
        }
        else {return 0;}
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryTableViewCell;
        let country = countries![indexPath.row];
       
        cell.countryName.text = country.name;
        //Background download of image using extension added in Network Request
        cell.CountryFlag.loadFromURL(photoUrl: "https://www.countryflags.io/\(country.code)/shiny/32.png");
        
        
        return cell;
    }
    
    //MARK: Table View Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        selectedCountryId = countries![indexPath.row].id.description
        selectedCountryName = countries![indexPath.row].name
        performSegue(withIdentifier: "showProvinces", sender: nil) // Perform segue after assigning values to variables being passed to next view controller
    }
}

// MARK: Country Table View Cell
class CountryTableViewCell: UITableViewCell {
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var CountryFlag: UIImageView!
    
}

struct Country {
       let id:NSInteger
       let name:String
       let code:String
       let phonecode:String?
   }


extension Country: Decodable { // Mapping json values to struct for use with JSON Decoder
     enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case code = "Code"
        case phonecode = "PhoneCode"
     }
 }




