//
//  Loading.swift
//  Countries
//
//  Created by Juacall Bernard on 3/11/20.
//  Copyright © 2020 mindbody. All rights reserved.
//

import Foundation
import UIKit

open class Loading{
    
    internal static var spinner: UIActivityIndicatorView?
    public static var style: UIActivityIndicatorView.Style = .large
    
    public static var baseBackColor = UIColor(white: 0.50, alpha: 0.6)
    public static var baseColor = UIColor.white
    
    
    public static func start(style: UIActivityIndicatorView.Style = style, backColor: UIColor = baseBackColor, baseColor: UIColor = baseColor) {
        DispatchQueue.main.async { // Added just in case this is not called on the main thread.
            if spinner == nil, let window = UIApplication.shared.keyWindow {
                let frame = UIScreen.main.bounds
                spinner = UIActivityIndicatorView(frame: frame)
                spinner!.backgroundColor = backColor
                spinner!.style = style
                spinner?.color = baseColor
                window.addSubview(spinner!)
                spinner!.startAnimating()
            }
        }
        
    }
    
    public static func stop() {
        DispatchQueue.main.async {
            if spinner != nil {
                spinner!.stopAnimating()
                spinner!.removeFromSuperview()
                spinner = nil
            }
        }
    }
}
