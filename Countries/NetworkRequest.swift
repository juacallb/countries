//
//  NetworkRequest.swift
//  Countries
//
//  Created by Juacall Bernard on 3/11/20.
//  Copyright © 2020 mindbody. All rights reserved.
//

import Foundation
import UIKit

protocol NetworkCallbackDelegate {
    // Classes that adopt this protocol must define these methods.
    
    //Type is for differentiating requests;
    func didFinishDownloading(_ data:Data, type:String)
    func didFinishWithError(_ error:Error, type:String)
}

class NetworkRequest {
    
    var delegate:NetworkCallbackDelegate
    //Create instance of network request with delegate
    
    init(delegate: NetworkCallbackDelegate ) {
        self.delegate = delegate
    }
    
    func getCountries(){
        
        let task = URLSession.shared.dataTask(with: URL(string:"https://connect.mindbodyonline.com/rest/worldregions/country")!){  (data,response,error) in
            if(error == nil){
                self.delegate.didFinishDownloading(data!, type: "countries");
            }else{
                self.delegate.didFinishWithError(error!, type: "countries");
            }
        }
        
        task.resume();
        
    }
    
    func getProvinces(countryID:String){
        let task = URLSession.shared.dataTask(with: URL(string:"https://connect.mindbodyonline.com/rest/worldregions/country/\(countryID)/province")!){  (data,response,error) in
                   if(error == nil){
                       self.delegate.didFinishDownloading(data!, type: "provinces");
                   }else{
                       self.delegate.didFinishWithError(error!, type: "provinces");
                   }
               }
               
               task.resume();
    }
}

//UIImageView extension for downloading images.
extension UIImageView {


    //load image async from internet
    func loadFromURL(photoUrl:String){
 
        let task =  URLSession.shared.dataTask(with: URL(string:photoUrl)!) { (data, response, error) -> Void in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }

        }
        task.resume()
    }


}
